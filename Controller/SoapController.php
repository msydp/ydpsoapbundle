<?php

namespace Ydp\SOAPBundle\Controller;

use BeSimple\SoapBundle\ServiceDefinition\Annotation as Soap;
use Symfony\Component\DependencyInjection\ContainerAware;

class SoapController extends ContainerAware
{
	/**
	 * @Soap\Method("helloWorld")
	 * @Soap\Param("name", phpType = "string")
	 * @Soap\Result(phpType = "string")
	 */
	public function helloWorld($name)
	{
		return $this->container->get('besimple.soap.response')->setReturnValue(sprintf('Hello %s!', $name));
	}
}
